#include "common.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netdb.h>
#include <iostream>
#include <string.h>
#include <string>
#include <map>

#define DEBUGPRINT(x) if(debugFlag){fprintf(stderr,x);}

#define BACKLOG 10

int debugFlag = 0;
int portNumber = 0;
int server_socket = 0;
int highest_socket = 0;

u_int8_t packetBuffer[PACKET_MAX_SIZE];

std::map<std::string, int> client_map;

void cleanup(int i) {
	std::map<std::string, int>::iterator it;
   for(it = client_map.begin(); it != client_map.end(); it++) {
      close(it->second);
   }
	close(server_socket);
	exit(i);
}


void handle_input(int argc, char **argv) {
   if (argc == 2) {
      portNumber = atoi(argv[1]);
   }
   else if (argc == 1) {
      portNumber = 0;
   }
   else {
      fprintf(stderr, "Usage %s [server-port]\n", argv[0]);
		cleanup(-1);
   }
}


int tcpServerSetup() {
	struct sockaddr_in6 server;      /* socket address for local side  */
	socklen_t len = sizeof(server);  /* length of local address        */
	/* create the tcp socket  */
	server_socket = socket(AF_INET6, SOCK_STREAM, 0);
	if(server_socket < 0){
		perror("socket call");
		cleanup(1);
	}
	server.sin6_family= AF_INET6;         		
	server.sin6_addr = in6addr_any;   //wild card machine address
	server.sin6_port= htons(portNumber);         
	/* bind the name (address) to a port */
	if (bind(server_socket, (struct sockaddr *) &server, sizeof(server)) < 0) {
		perror("bind call");
		cleanup(-1);
	}
	//get the port name and print it out
	if (getsockname(server_socket, (struct sockaddr*)&server, &len) < 0) {
		perror("getsockname call");
		cleanup(-1);
	}
	if (listen(server_socket, BACKLOG) < 0) {
		perror("listen call");
		cleanup(-1);
	}
	printf("Server is using port %d \n", ntohs(server.sin6_port));
	return server_socket;
}


void insertNewMap(int socket, target *packet) {
	chat_header reply;
	reply.PDU_length = htons(sizeof(chat_header));
	std::string handle = std::string((char *)(packet->handle), packet->length);
	std::map<std::string, int>::iterator search = client_map.find(handle);
	if (search == client_map.end()) {
		client_map.insert(std::pair<std::string, int>(handle, socket));
		reply.flag = 2;
	}
	else if (search->second == 0) {
		client_map.at(handle) = socket;
		reply.flag = 2;
	}
	else {
		reply.flag = 3;
	}
	printf("Client %s connected on %d\n", handle.c_str(), socket);
	DEBUGPRINT("Sending Reply Packet\n");
	sendPacket(socket, (char *)&reply, sizeof(chat_header), 0);
}


void broadcastPacket(int src_socket, broadcast *packet) {
	std::map<std::string, int>::iterator it;	
	for(it = client_map.begin(); it != client_map.end(); it++) {
		if(it->second != src_socket) {
			sendPacket(it->second, (char *)packet, ntohs(packet->header.PDU_length), 0);
		}
	}
}


void messagePacket(int socket, multicast *packet) {
	for (int i = 0; i < packet->des_count; i++) {
		std::string des = std::string((char *)(packet->des[i].handle),packet->des[i].length);
		std::map<std::string, int>::iterator search = client_map.find(des);
		if (search != client_map.end()) {
			sendPacket(search->second, (char *)packet, sizeof(multicast), 0);
			DEBUGPRINT("Message Forwarded\n");
		}
		else {
			//Error packet, destination not found
			header_handle h_packet;
			h_packet.header.PDU_length = htons(sizeof(header_handle));
			h_packet.header.flag = 7;
			h_packet.handle.length = packet->des[i].length;
			strncpy((char *)(h_packet.handle.handle), (char *)(packet->des[i].handle), HANDLE_LENGTH);
			sendPacket(socket, (char *)&h_packet, sizeof(header_handle), 0);
			DEBUGPRINT("Handle not found reply sent.\n");
		}
	}
}


void removeClient(int socket) {
	std::map<std::string, int>::iterator it;	
	for(it = client_map.begin(); it != client_map.end(); it++) {
		if (it->second == socket) {
			break;
		}
	}
	if (it != client_map.end()) {
		close(it->second);
		client_map.erase(it);
	}
}


void exitClient(int socket) {
	chat_header reply;
	reply.PDU_length = htons(sizeof(chat_header));
	reply.flag = 9;

	sendPacket(socket, (char *)&reply, sizeof(chat_header), 0);
	removeClient(socket);
}


void listPacket(int socket) {
	//send reply packet
	handle_count reply;
	reply.header.PDU_length = htons(sizeof(handle_count));
	reply.header.flag = 11;
	//count number of active users
	int client_count = 0;
	std::map<std::string, int>::iterator it;	
	for(it = client_map.begin(); it != client_map.end(); it++) {
		if (it->second != 0) {
			client_count++;
		}
	}
	reply.handle_count = htonl(client_count);
	sendPacket(socket, (char *)&(reply), sizeof(handle_count), 0);
	//send one 12 packet for each user connected.
	header_handle clientPacket;
	clientPacket.header.PDU_length = htons(sizeof(header_handle));
	clientPacket.header.flag = 12;	
	for(it = client_map.begin(); it != client_map.end(); it++) {
		if (it->second != 0) {
			clientPacket.handle.length = (it->first).length();
			strncpy((char *)(clientPacket.handle.handle), it->first.c_str(), HANDLE_LENGTH);
			sendPacket(socket, (char *)&clientPacket, sizeof(header_handle), 0);
		}
	}
	//Ending list packet
	chat_header tailPacket;
	tailPacket.PDU_length = htons(sizeof(chat_header));
	tailPacket.flag = 13;
	sendPacket(socket, (char *)&tailPacket, sizeof(chat_header), 0);
}


void parsePacketServer(int socket) {
	if (recvPacket(socket, (char *)packetBuffer) == -1) {
		removeClient(socket);
	}
	DEBUGPRINT("Packet Received\n");
	switch(((chat_header *) packetBuffer)->flag) {
		case 4:
			DEBUGPRINT("Received Broadcast Packet\n");
			broadcastPacket(socket, (broadcast *)packetBuffer);
			break;
		case 5:
			DEBUGPRINT("Received Message Packet\n");
			messagePacket(socket, (multicast *)packetBuffer);
			break;
		case 8:
			DEBUGPRINT("Received Exit Packet\n");
			exitClient(socket);
			break;
		case 10:
			DEBUGPRINT("Received List Packet\n");
			listPacket(socket);
			break;
		default:
			perror("Bad Packet");
			break;
	}
}


void tcpAccept()
{
	struct sockaddr_in6 clientInfo;   
	int clientInfoSize = sizeof(clientInfo);
	int client_socket= 0;
	//accept new connection
	if ((client_socket = accept(server_socket, (struct sockaddr*) &clientInfo, (socklen_t *) &clientInfoSize)) < 0) {
		perror("accept call");
		exit(-1);
	}
	if (debugFlag) {
		printf("Client accepted.  Client IP: %s Client Port Number: %d\n",  
				getIPAddressString(clientInfo.sin6_addr.s6_addr), ntohs(clientInfo.sin6_port));
	}
	//get initial packet from client ad reply with 2 or 3
	if (recvPacket(client_socket, (char *)packetBuffer) == -1) {
		return;
	}
	insertNewMap(client_socket, &(((header_handle *)packetBuffer)->handle));
	return;
}


void processSelect() {
	fd_set readfs;
	int max_fd = server_socket;
	//clear and add server to the FD
	FD_ZERO(&readfs);
	FD_SET(server_socket, &readfs);
	//add exisiting clients to the FD
	std::map<std::string, int>::iterator it;	
	for(it = client_map.begin(); it != client_map.end(); it++) {
		FD_SET(it->second, &readfs);
		if (max_fd < (it->second)) {
			max_fd = it->second;
		}
	}
	//run select
	int select_ret = select(max_fd + 1, &readfs, NULL, NULL, 0);
	if (select_ret < 0) {
		perror("Select Failure");
		cleanup(-1);
	}
	// NEW CLIENT
	if (FD_ISSET(server_socket, &readfs)) {
		tcpAccept();
		DEBUGPRINT("New Client added!\n");
	}
	// OLD CLIENTS
	if (select_ret > 0) {
		//Loop to find the socket that set the FD
		std::map<std::string, int>::iterator it;
		for(it = client_map.begin(); it != client_map.end(); it++) {
			if (FD_ISSET(it->second, &readfs)) {
				DEBUGPRINT("Incoming packet.\n");
				parsePacketServer(it->second);
   		}
		}
	}
}

int main(int argc, char **argv) {
   handle_input(argc, argv);
   tcpServerSetup();

   while(true) {
		processSelect();
   }
	return 0;
}
