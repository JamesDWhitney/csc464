#include "common.h"

#include <string.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <cctype>
#include <vector>
#include <sstream>

#define DEBUGPRINT(x) if(debugFlag){printf(x);}

using namespace std;

int debugFlag = 0;
int socketNum = 0;
int handle_length = 0;
char handle[HANDLE_LENGTH];
char server_name[HANDLE_LENGTH];
int server_port;

u_int8_t packetBuffer[PACKET_MAX_SIZE];


void cleanup(int i) {
	close(socketNum);
	exit(i);
}


void handle_input(int argc, char **argv) {
   if(argc != 4){
      fprintf(stderr, "Usage %s handle server-name server-port\n", argv[0]);
		cleanup(-1);
   }
	if (strlen(argv[1]) > HANDLE_LENGTH) {
		fprintf(stderr, "Invalid handle, handle longer than 100 characters: %s", argv[1]);
		cleanup(-1);
	}
   strncpy(handle, argv[1], HANDLE_LENGTH);
	handle_length = strlen(argv[1]);
	if (isdigit(handle[0])) {
		fprintf(stderr, "Invalid handle, handle starts with a number\n");
		cleanup(-1);
	}
   strncpy(server_name, argv[2], HANDLE_LENGTH);
   server_port = atoi(argv[3]);
}


void tcpClientSetup() {
	// This is used by the client to connect to a server using TCP
	uint8_t * ipAddress = NULL;
	struct sockaddr_in6 server;      
	
	// create the socket
	if ((socketNum = socket(AF_INET6, SOCK_STREAM, 0)) < 0) {
		perror("socket call");
		cleanup(-1);
	}
	// setup the server structure
	server.sin6_family = AF_INET6;
	server.sin6_port = htons(server_port);
	// get the address of the server 
	if ((ipAddress = getIPAddress6(server_name, &server)) == NULL) {
		cleanup(-1);
	}
	if(connect(socketNum, (struct sockaddr*)&server, sizeof(server)) < 0) {
		perror("connect call");
		cleanup(-1);
	}
	if (debugFlag) {
		printf("Connected to %s IP: %s Port Number: %d\n", server_name, getIPAddressString(ipAddress), server_port);
	}
}


void annouceToServer() {
	header_handle annoucePacket;
	annoucePacket.header.PDU_length = htons(sizeof(header_handle));
	annoucePacket.header.flag = 1;
	annoucePacket.handle.length = handle_length;
	strncpy((char *)&(annoucePacket.handle.handle), handle, HANDLE_LENGTH);
	//send flag 1 packet
	DEBUGPRINT("sending annouce packet\n");
	sendPacket(socketNum, (char *)&annoucePacket, sizeof(header_handle), 0);
	//wait for flag 2 or 3 ACK
	DEBUGPRINT("recv ACK packet\n");
	if (recvPacket(socketNum, (char *)packetBuffer) == -1) {
		cleanup(-1);
	}
	DEBUGPRINT("recved packet\n");
	if (((chat_header *)packetBuffer)->flag == 3) {
		DEBUGPRINT("ACK packet Bad\n");
		fprintf(stderr, "Handle already in use: <%s>", handle);
		cleanup(-1);
	}
	if (((chat_header *)packetBuffer)->flag != 2) {
		fprintf(stderr,"Failed to receive handle confirmation");
		cleanup(-1);
	}
	DEBUGPRINT("ACK packet GOOD\n");
}


void getHandleList() {
	handle_count *packet = (handle_count *)packetBuffer;
	int handleCount = ntohl(packet->handle_count);
	printf("Number of clients: %d\n", handleCount);
	while (true) {
		recvPacket(socketNum, (char *)packetBuffer);
		if (((chat_header *)packetBuffer)->flag == 13) {
			break;
		}
		else if (((chat_header *)packetBuffer)->flag == 12) {
			printf("\t%s\n", 
			std::string((char *)(((header_handle *)packetBuffer)->handle.handle),
							 ((header_handle *)packetBuffer)->handle.length).c_str());
		}
	}
}


void parsePacketClient() {
	if (recvPacket(socketNum, (char *)packetBuffer) == -1) {
		printf("Server Terminated\n");
		cleanup(1);
	}
	switch(((chat_header *)packetBuffer)->flag) {
		case 4:
			printf("\n%s: %s\n",
				std::string((char *)(((broadcast *)packetBuffer)->src.handle),
							   ((broadcast *)packetBuffer)->src.length).c_str(), 
				std::string((char *)(((broadcast *)packetBuffer)->message)).c_str());
			break;
		case 5:
			printf("\n%s: %s\n",
				std::string((char *)(((multicast *)packetBuffer)->src.handle),
								((multicast *)packetBuffer)->src.length).c_str(), 
				std::string((char *)(((multicast *)packetBuffer)->message)).c_str());
			break;
		case 7:
			printf("\nClient with handle %s does not exist.\n", 
				std::string((char *)(((header_handle *)packetBuffer)->handle.handle),
								((header_handle *)packetBuffer)->handle.length).c_str());
			break;
		case 9:
			printf("\nExiting...\n");
			cleanup(1);
			break;
		case 11:
			getHandleList();
			break;
		default:
			fprintf(stderr,"Bad Packet\n");
			break;
	}
}


void sendMessage(std::string line) {
	DEBUGPRINT("sending message\n");
	std::stringstream commands(line);
	std::string one;
	//ditch %m
	commands >> one;
	commands >> one;
	bool multi = true;
	int numHandles = 1;
	if (isdigit(one[0])) {
		DEBUGPRINT("Mutiple Des option found\n");
		numHandles = atoi(one.c_str());
		if (numHandles > 9 || numHandles < 1) {
			fprintf(stderr, "%%M is invalid\n");
			return;
		}
	}
	else {
		multi = false;
	}
	//Build message packet
	multicast packet;
	packet.header.PDU_length = htons(sizeof(multicast));
	packet.header.flag = 5;
	packet.src.length = handle_length;
	strncpy((char *)&(packet.src.handle), (char *)handle, handle_length);
	packet.des_count = numHandles;
	if (multi) {
		commands >> one;
	}
	//if not multi then one is the des handle
	packet.des[0].length = one.length();
	strncpy((char *)&(packet.des[0].handle), 
				(char *)one.c_str(),
				one.length());
	if (multi) {
		std::string temp;
		for(int i = 1; i < numHandles; i++) {
			commands >> temp;
			if (temp.length() == 0) {
				fprintf(stderr, "Invalid Command\n");
				return;
			}
			packet.des[i].length = temp.length();
			strncpy((char *)&(packet.des[i].handle), 
				(char *)temp.c_str(),
				temp.length());
		}
	}
	std::string message;
	if (commands.eof()) {
		message = "\n";
	}
	else {
		message = line.substr(commands.tellg());
		if (message.length() == 0) {
			message = "\n";
		}
	}
	
	while (message.length() > MESSAGE_LENGTH) {
		strncpy( (char *)&(packet.message),
			message.substr(0, MESSAGE_LENGTH-1).c_str(),
			MESSAGE_LENGTH);
		message = message.substr(MESSAGE_LENGTH);
		sendPacket(socketNum, (char *)&packet, sizeof(multicast), 0);
		DEBUGPRINT("Big Message Sent\n");
	}
	strncpy( (char *)&(packet.message),
		message.c_str(),
		message.length()+1);
	sendPacket(socketNum, (char *)&packet, sizeof(multicast), 0);
	DEBUGPRINT("Message Sent COMPLETE\n");
}


void sendBroadcast(std::string line) {
	DEBUGPRINT("sending broadcast\n");
	std::stringstream commands(line);
	std::string message;
	//ditch %b
	commands >> message;
	if (commands.eof()) {
		message = "\n";
	}
	else{
		message = line.substr(commands.tellg());
		if (message.length() == 0) {
			message = "\n";
		}
	}
	broadcast packet; 
	packet.header.PDU_length = htons(sizeof(broadcast));
	packet.header.flag = 4;
	packet.src.length = handle_length;
	strncpy((char *)&(packet.src.handle), (char *)handle, handle_length);
	while (message.length() > MESSAGE_LENGTH) {
		strncpy( (char *)&(packet.message),
			message.substr(0, MESSAGE_LENGTH-1).c_str(),
			MESSAGE_LENGTH);
		message = message.substr(MESSAGE_LENGTH);
		sendPacket(socketNum, (char *)&packet, sizeof(broadcast), 0);
		DEBUGPRINT("Big Broadcast Sent\n");
	}
	strncpy( (char *)&(packet.message),
		message.c_str(),
		message.length()+1);
	sendPacket(socketNum, (char *)&packet, sizeof(broadcast), 0);
	DEBUGPRINT("Broadcast Sent COMPLETE\n");
}


void sendList() {
	DEBUGPRINT("sending listreq\n");
	chat_header packet;
	packet.PDU_length = htons(sizeof(chat_header));
	packet.flag = 10;
	sendPacket(socketNum, (char *)&packet, sizeof(chat_header), 0);
}


void sendExit() {
	DEBUGPRINT("sending exit\n");
	chat_header packet;
	packet.PDU_length = htons(sizeof(chat_header));
	packet.flag = 8;
	sendPacket(socketNum, (char *)&packet, sizeof(chat_header), 0);
}


void processCommand() {
	std::string line;
	getline(cin, line);
	std::stringstream ss(line);
	std::string i;
	ss >> i;
	if (i.compare("%M") == 0 || i.compare("%m") == 0) {
		if (ss.eof()) {
			fprintf(stderr, "Invalid Command\n");
			return;
		}
		sendMessage(line);
		return;
	}
	else if (i.compare("%B") == 0 || i.compare("%b") == 0) {
		sendBroadcast(line);
		return;
	}
	else if (i.compare("%L") == 0 || i.compare("%l") == 0) {
		sendList();
		return;
	}
	else if (i.compare("%E") == 0 || i.compare("%e") == 0) {
		sendExit();
		return;
	}
	fprintf(stderr, "Invalid Command\n");
}


void processSelect() {
	fd_set readfs;
	int max_fd = socketNum;
	FD_ZERO(&readfs);
	FD_SET(socketNum, &readfs);
	FD_SET(STDIN_FILENO, &readfs);

	int select_ret = select(max_fd + 1, &readfs, NULL, NULL, 0);
	if (select_ret < 0) {
		perror("Select Failure");
		cleanup(-1);
	}
	if (select_ret > 0) {
		if (FD_ISSET(socketNum, &readfs)) {
			DEBUGPRINT("Incoming packet.\n");
			parsePacketClient();
		}
		else if (FD_ISSET(STDIN_FILENO, &readfs)) {
			processCommand();
		}
	}
}


int main(int argc, char **argv){
	printf("$: ");
	fflush(stdout);
   //take in input and set globals
   handle_input(argc, argv);
   //Connection Setup
   tcpClientSetup();
	annouceToServer();
   while(true) {
		processSelect();
		printf("$: ");
		fflush(stdout);
   }
   cleanup(0);
	return 0;
}