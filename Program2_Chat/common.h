#ifndef _COMMON_H
#define _COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define PACKET_MAX_SIZE 1400
#define MESSAGE_LENGTH 200
#define HANDLE_LENGTH 100
#define MAX_HANDLES 9

//Define Packet Structures

typedef struct {
   u_int16_t   PDU_length;
   u_int8_t    flag;
}__attribute__((packed)) chat_header; //2 3 8 9 10


typedef struct {
   u_int8_t    length;
   u_int8_t    handle[HANDLE_LENGTH];
}__attribute__((packed)) target;


typedef struct {
   chat_header header;
   target      src;
   u_int8_t    message[MESSAGE_LENGTH];
}__attribute__((packed)) broadcast;


typedef struct {
   chat_header header;
   target      src;
   u_int8_t    des_count;
   target      des[MAX_HANDLES];
   u_int8_t    message[MESSAGE_LENGTH];
}__attribute__((packed)) multicast;


typedef struct {
   chat_header header;
   target      handle;
}__attribute__((packed)) header_handle; //1 7 12


typedef struct {
   chat_header    header;
   u_int32_t      handle_count;
}__attribute__((packed)) handle_count;


void sendPacket(int socket, char *buf, int size, int flag);
int recvPacket(int socket, char *buf);
uint8_t * gethostbyname6(const char * hostName);
char * getIPAddressString(uint8_t * ipAddress);
uint8_t * getIPAddress6(const char * hostName, struct sockaddr_in6 * aSockaddr);
void parsePacket(int socket);


#endif