/* Basecode written by Hugh Smith of CalPoly 
   Minor changes made by me, James Whitney*/

#ifndef __SREJ_H__
#define __SREJ_H__

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "networks.h"
#include "cpe464.h"

#define DEBUG(X) fprintf(stderr,"%s\n",X);

#define MAX_LEN 1500
#define SIZE_OF_BUF_SIZE 4
#define MAX_FILENAME_SIZE 100
#define START_SEQ_NUM 1
#define MAX_TRIES 10
#define LONG_TIME 10
#define SHORT_TIME 1


#pragma pack(1)
typedef struct Header {
   uint32_t seq_num;
   uint16_t checksum;
   uint8_t flag;
}Header;


#pragma pack(1)
typedef struct FnameBody {
   char fname[MAX_FILENAME_SIZE];
   int32_t buf_size;
   int32_t win_size;
}FnameBody;


#pragma pack(1)
typedef struct Packet {
   uint8_t packet[MAX_LEN];
   int32_t length;
}Packet;


#pragma pack(1)
typedef struct Window {
   Packet * packets;
   int32_t size;
   uint32_t min;
   uint32_t seq_num;
}Window;


#define DATA_FLAG 3
#define RR_FLAG   5
#define SREJ_FLAG   6
#define FNAME_FLAG 7
#define FNAME_RES 8
#define EOF_FLAG 9
#define EOF_ACK_FLAG 10

#define FNAME_BAD_FLAG 0
#define FNAME_GOOD_FLAG 1


enum FLAG {
   FNAME, DATA, FNAME_OK, FNAME_BAD, ACK, END_OF_FILE, EOF_ACK, CRC_ERROR = -1
};


int32_t send_buf(uint8_t * buf, uint32_t len, Connection * connection, uint8_t flag, uint32_t seq_num, uint8_t * packet);
int createHeader(uint32_t len, uint8_t flag, uint32_t seq_num, uint8_t * packet);
int32_t recv_buf(uint8_t * buf, int32_t len, int32_t recv_sk_num, Connection * connection, uint8_t * flag, uint32_t * seq_num);
int retrieveHeader(char * data_buf, int recv_len, uint8_t * flag, uint32_t * seq_num);
int processSelect(Connection * client, int * retryCount, int selectTimeoutState, int dataReadyState, int doneState);

#endif
