/* Basecode written by Hugh Smith of CalPoly for stop and wait protocol
   Myself, James Whitney, modified the following to support 
   sliding window selective reject.
   */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "networks.h"
#include "srej.h"

#include "cpe464.h"

typedef enum State STATE;

enum State
{
   START,
   DONE,
   FILENAME, 
   RECV_DATA,
   WRITE_OUT,
};


void process_server(int server_sk_num, float error_rate);
void process_client(int32_t server_sk_num, float error_rate, uint8_t * buf, int32_t recv_len, Connection * client);
STATE filename(Connection * client, uint8_t * buf, int32_t recv_len,
               int32_t * data_file, int32_t * buf_size, Window * window);
STATE recv_data(int32_t output_file, Connection * client, Window * window);
STATE file_ok(int * output_file_fd, char * outputFileName);
int processArgs(int argc, char ** argv);


int main( int argc, char *argv[])
{
   int32_t server_sk_num = 0;
   int portNumber = 0;

   portNumber = processArgs(argc, argv);
   float error_rate = atof(argv[1]);
   
   // set up the main server portNumber
   server_sk_num = udp_server(portNumber);
   process_server(server_sk_num, error_rate);
   return 0;
}


void process_server(int server_sk_num, float error_rate)
{
   pid_t pid = 0;
   int status = 0;
   uint8_t buf[MAX_LEN];
   Connection client;
   uint8_t flag = 0;
   uint32_t seq_num = 0;
   int32_t recv_len = 0;

   while(1) {
      if(select_call(server_sk_num, LONG_TIME, 0, SET_NULL) == 1) {
         recv_len = recv_buf(buf, MAX_LEN, server_sk_num, &client, &flag, &seq_num);
         if (recv_len != CRC_ERROR) {
            if ((pid = fork()) < 0) {
               perror("fork");
               exit(-1);
            }
            //child
            if (pid == 0) {
               process_client(server_sk_num, error_rate, buf, recv_len, &client);
               exit(0);
            }
         }
         //check to see if any children quit
         while (waitpid(-1, &status, WNOHANG) > 0) {
         }
      }
   }
}


void process_client(int32_t server_sk_num, float error_rate, uint8_t * buf, int32_t recv_len, Connection * client)
{
   //server error_rate 
   sendtoErr_init(error_rate, DROP_ON, FLIP_ON, DEBUG_ON, RSEED_ON);
   STATE state = START;
   int32_t output_file_fd = 0;

   int32_t buf_size = 0;
   int32_t win_size = 0;

   Window window;
   window.min = START_SEQ_NUM;
   window.seq_num = START_SEQ_NUM;

   while(state != DONE) {
      switch (state) {
      case START:
         state = FILENAME;
         break;

      case FILENAME:
         state = filename(client, buf, recv_len, &output_file_fd, &buf_size, &window);
         break;

      case RECV_DATA:
         state = recv_data(output_file_fd, client, &window);
         break;

      case DONE:
         close(output_file_fd);
         break;

      default:
         printf("In default and you should not be here!!!!\n");
         state = DONE;
         break;
      }
   }
}


STATE filename(Connection * client, uint8_t * buf, int32_t recv_len, 
               int32_t * output_file_fd, int32_t * buf_size, Window * window)
{
   uint8_t response;
   STATE returnValue = DONE;

   FnameBody * fnamebody = (FnameBody *) buf;
   *buf_size = ntohl(fnamebody->buf_size);
   window->size = ntohl(fnamebody->win_size);

   window->packets = calloc(window->size, sizeof(Packet));
   if (window->packets == NULL) {
      fprintf(stderr, "Calloc Failure.\n");
      exit(-1);
   }

   if ((client->sk_num = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
      perror("filename, open client socket");
      exit(-1);
   }
   if (((*output_file_fd) = open(fnamebody->fname, O_CREAT | O_TRUNC | O_WRONLY, 0600)) < 0) {
      perror("File open error:");
      response = FNAME_BAD_FLAG;
      returnValue = DONE;
   }
   else {
      response = FNAME_GOOD_FLAG;
      returnValue = RECV_DATA;
   }
   send_buf(&response, sizeof(uint8_t), client, FNAME_RES, 0, buf);
   return returnValue;
}


STATE recv_data(int32_t output_file, Connection * client, Window * window)
{
   uint32_t seq_num = 0;
   uint8_t flag = 0;
   int32_t data_len = 0;
   Packet * win_packet;
   uint8_t data_buf[MAX_LEN];
   uint8_t packet[MAX_LEN];
   static int retryCount = 0;
   uint32_t rr = 0;
   uint32_t srej_num = 0;
   static uint32_t last_srej = 0;

   STATE returnValue = DONE;

   if ((returnValue = processSelect(client, &retryCount, RECV_DATA, RECV_DATA, DONE)) == RECV_DATA) {
      data_len = recv_buf(data_buf, MAX_LEN, client->sk_num, client, &flag, &seq_num);
      
      if (data_len == CRC_ERROR) {
         returnValue = RECV_DATA;
      }
      else if (seq_num == window->min) {
         (window->min)++;

         if (flag == EOF_FLAG) {
            send_buf(packet, 1, client, EOF_ACK_FLAG, window->seq_num, packet);
            (window->seq_num)++;
            returnValue = DONE;
         }
         else if (flag == DATA_FLAG) {
            write(output_file, &data_buf, data_len);
            while(window->packets[window->min % window->size].length > 0) {
               win_packet = &(window->packets[window->min % window->size]);
               write(output_file, win_packet->packet, win_packet->length);
               win_packet->length = 0;
               (window->min)++;
            }
            rr = htonl(window->min);
            send_buf((uint8_t *)&rr, sizeof(uint32_t), client, RR_FLAG, window->seq_num, packet);
            (window->seq_num)++;
            returnValue = RECV_DATA;
         }
         else {
            fprintf(stderr, "Rouge Packet, not a EOF or DATA packet (this should never happen) is a %d\n", flag);
            returnValue = DONE;
         }
      }
      else if (seq_num > window->min){
         win_packet = &(window->packets[seq_num % window->size]);
         memcpy(win_packet->packet, data_buf, data_len);
         win_packet->length = data_len;

         if (window->min == last_srej) {
            rr = htonl(window->min);
            send_buf((uint8_t *)&rr, sizeof(uint32_t), client, RR_FLAG, window->seq_num, packet);
            (window->seq_num)++;
            returnValue = RECV_DATA;
         }else {
            last_srej = window->min;
            srej_num = htonl(window->min);
            send_buf((uint8_t *)&srej_num, sizeof(uint32_t), client, SREJ_FLAG, window->seq_num, packet);
            (window->seq_num)++;
            returnValue = RECV_DATA;
         }
      }
      else {
         rr = htonl(window->min);
         send_buf((uint8_t *)&rr, sizeof(uint32_t), client, RR_FLAG, window->seq_num, packet);
         (window->seq_num)++;
         returnValue = RECV_DATA;
      }
   }
   return returnValue;
}


int processArgs(int argc, char ** argv)
{
   int portNumber = 0;
   if (argc < 2 || argc > 3) {
      printf("Usage %s error_rate [port number]\n", argv[0]);
      exit(-1);
   }
   if (argc == 3) {
      portNumber = atoi(argv[2]);
   }
   else {
      portNumber = 0;
   }
   return portNumber;
}
