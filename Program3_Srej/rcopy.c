/* Basecode written by Hugh Smith of CalPoly for stop and wait protocol
   Myself, James Whitney, modified the following to support 
   sliding window selective reject.
   */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "networks.h"
#include "srej.h"

#include "cpe464.h"

typedef enum State STATE;

enum State
{
   START_STATE,
   FILENAME,
   SEND_DATA,
   CHECK_SELECT,
   RESEND_PACKET,
   WAIT_ON_WINDOW,
   WAIT_ON_EOF_ACK,
   DONE
};


STATE start_state(char ** argv, Connection * server);
STATE filename(char * fname, int32_t buf_size, int32_t win_size, Connection * server);

void check_args(int argc, char ** argv);

STATE send_data(Connection * server, Window * window, int32_t data_file, int32_t buf_size, int * retryCount, uint8_t * fileComplete, uint32_t * lastSeq);
STATE resend_packet(Connection * server, Window * window, uint32_t seq_num, int * retryCount);
STATE check_select(Connection * server, Window * window, uint32_t * srej_num, int * retryCount, uint8_t * fileComplete, uint32_t * lastSeq);
STATE wait_on_window(Connection * server, Window * window, uint32_t * srej_num, uint8_t * fileComplete, uint32_t * lastSeq);
STATE wait_on_eof_ack(Connection * server, Window * window);


int main( int argc, char * argv[] )
{
   Connection server;
   STATE state = START_STATE;
   check_args(argc, argv);

   int32_t input_fd = 0;
   int32_t buf_size = atoi(argv[4]);
   uint32_t srej_num = 0;
   uint8_t fileComplete = 0;
   uint32_t lastSeq = 0;

   static int retryCount = 0;

   Window window;
   window.size = atoi(argv[3]);
   window.min = START_SEQ_NUM;
   window.seq_num = START_SEQ_NUM;

   window.packets = calloc(window.size, sizeof(Packet));
   if (window.packets == NULL) {
      fprintf(stderr, "Calloc Failure.\n");
      exit(-1);
   }

   if (((input_fd) = open(argv[1], O_RDONLY)) < 0) {
      perror("Unable to open input file.");
      exit(-1);
   }

   sendtoErr_init(atof(argv[5]), DROP_ON, FLIP_ON, DEBUG_ON, RSEED_ON);

   while (state != DONE) {
      switch (state) {
         case START_STATE:
            state = start_state(argv, &server);
            break;

         case FILENAME:
            state = filename(argv[2], buf_size, window.size, &server);
            break;

         case SEND_DATA:
            state = send_data(&server, &window, input_fd, buf_size, &retryCount, &fileComplete, &lastSeq);
            break;

         case CHECK_SELECT:
            state = check_select(&server, &window, &srej_num, &retryCount, &fileComplete, &lastSeq);
            break;            

         case RESEND_PACKET:
            state = resend_packet(&server, &window, srej_num, &retryCount);
            break;            

         case WAIT_ON_WINDOW:
            state = wait_on_window(&server, &window, &srej_num, &fileComplete, &lastSeq);
            break;

         case WAIT_ON_EOF_ACK:
            state = wait_on_eof_ack(&server, &window);
            break;

        case DONE:
            close(input_fd);
            close(server.sk_num);
            break;

         default:
            fprintf(stderr, "ERROR - in default state\n");
            break;
      }
   }
   return 0;
}


STATE start_state(char ** argv, Connection * server)
{
   STATE returnValue = FILENAME;

   if (server->sk_num > 0) {
      close(server->sk_num);
   }
   if (udp_client_setup(argv[6], atoi(argv[7]), server) < 0) {
      returnValue = DONE;
   }
   else {
      returnValue = FILENAME;
   }
   return returnValue;
}


STATE filename(char * fname, int32_t buf_size, int32_t win_size, Connection * server)
{
   int returnValue = START_STATE;
   uint8_t packet[MAX_LEN];
   uint8_t flag = 0;
   uint32_t seq_num = 0;
   int32_t fname_len = strlen(fname) + 1;
   int32_t recv_check = 0;
   static int retryCount = 0;

   FnameBody fnamebody;
   memcpy(fnamebody.fname, fname, fname_len);
   fnamebody.buf_size = htonl(buf_size);
   fnamebody.win_size = htonl(win_size);

   send_buf((uint8_t *) &fnamebody, sizeof(FnameBody), server, FNAME_FLAG, 0, packet);

   if ((returnValue = processSelect(server, &retryCount, START_STATE, SEND_DATA, DONE)) == SEND_DATA) {
      recv_check = recv_buf(packet, MAX_LEN, server->sk_num, server, &flag, &seq_num);
      if (recv_check == CRC_ERROR) {
         returnValue = START_STATE;
      }
      else if (flag == FNAME_BAD_FLAG) {
         fprintf(stderr, "Error during file open of %s on server\n", fname);
         returnValue = DONE;
      }
   }
   return(returnValue);
}


STATE send_data(Connection * server, Window * window, int32_t data_file, int32_t buf_size, int * retryCount, uint8_t * fileComplete, uint32_t * lastSeq)
{
   Packet * packet = &(window->packets[window->seq_num % window->size]);
   uint8_t buf[MAX_LEN];
   int32_t len_read = 0;
   STATE returnValue = DONE;

   if (!(*fileComplete)){
      if ((*retryCount) > MAX_TRIES) {
         return returnValue;
      }
      len_read = read(data_file, buf, buf_size);
      switch(len_read) {
      case -1:
         perror("send_data, read error");
         returnValue = DONE;
         break;
      case 0:
         (*fileComplete) = 1;
         (*lastSeq) = window->seq_num;
         returnValue = CHECK_SELECT;
         break;
      default:
         packet->length = send_buf(buf, len_read, server, DATA_FLAG, window->seq_num, packet->packet);
         (*retryCount)++;
         (window->seq_num)++;
         returnValue = CHECK_SELECT;
         break;
      }
      return returnValue;
   }
   else {
      return CHECK_SELECT;
   }
}


STATE resend_packet(Connection * server, Window * window, uint32_t seq_num, int * retryCount)
{
   Packet * packet = &(window->packets[seq_num % window->size]);
   if (safeSend(packet->packet, packet->length, server) < 0) {
      perror("Resend timeout");
      exit(-1);
   }
   (*retryCount)++;
   return CHECK_SELECT;
}


STATE check_select(Connection * server, Window * window, uint32_t * srej_num, int * retryCount, uint8_t * fileComplete, uint32_t * lastSeq)
{
   uint8_t buf[MAX_LEN];
   int32_t len = MAX_LEN;
   uint8_t flag = 0;
   uint32_t seq_num = 0;
   uint32_t crc_check = 0;
   uint32_t rr = 0;
   STATE returnValue = DONE;

   if (select_call(server->sk_num, 0, 0, NOT_NULL) == 1) {
      crc_check = recv_buf(buf, len, server->sk_num, server, &flag, &seq_num);
      (*retryCount) = 0;
      if (crc_check == CRC_ERROR) {
         returnValue = CHECK_SELECT;
      }
      else if (flag == RR_FLAG){
         rr = ntohl(*(uint32_t *)buf);
         if (rr > window->min) {
            window->min = rr;
         }
         if (*fileComplete && (rr == *lastSeq)) {
            return WAIT_ON_EOF_ACK;
         }
         returnValue = CHECK_SELECT;
      }
      else if (flag == SREJ_FLAG) {
         (* srej_num) = ntohl(*(uint32_t *)buf);
         returnValue = RESEND_PACKET;
      }
      else {
         printf("In check_select but its not an return flag (this should never happen) is %d\n", flag);
         returnValue = DONE;
      }
   }
   else{
      returnValue = WAIT_ON_WINDOW;
   }
   return returnValue;
}


STATE wait_on_window(Connection * server, Window * window, uint32_t * srej_num, uint8_t * fileComplete, uint32_t * lastSeq)
{
   STATE returnValue = DONE;   
   static int retryCount = 0;
   if ( !(*fileComplete) && window->seq_num < (window->min + window->size)) {
      returnValue = SEND_DATA;
   }
   else {
      if ((returnValue = processSelect(server, &retryCount, RESEND_PACKET, CHECK_SELECT, DONE)) == RESEND_PACKET) {
         (* srej_num) = window->min;
      }
   }
   return returnValue;
}


STATE wait_on_eof_ack(Connection * server, Window * window)
{
   STATE returnValue = DONE;
   uint32_t crc_check = 0;
   uint8_t buf[MAX_LEN];
   int32_t len = MAX_LEN;
   uint8_t flag = 0;
   uint32_t seq_num = 0;
   static int retryCount = 0;

   if (retryCount > MAX_TRIES) {
      printf("Sent data %d times, no ACK, server is probably gone terminating...", MAX_TRIES);
      return DONE;
   }

   send_buf(buf, 1, server, EOF_FLAG, window->seq_num, buf);

   if ((returnValue = processSelect(server, &retryCount, WAIT_ON_EOF_ACK, DONE, DONE)) == DONE) {
      crc_check = recv_buf(buf, len, server->sk_num, server, &flag, &seq_num);

      if (crc_check == CRC_ERROR) {
         returnValue = WAIT_ON_EOF_ACK;
      }
      else if (flag == RR_FLAG) {
         returnValue = WAIT_ON_EOF_ACK;
      }
      else if (flag != EOF_ACK_FLAG){
         printf("In wait_on_eof_ack but its not an EOF_ACK flag (this should never happen) is %d\n", flag);
         returnValue = DONE;
      }
      else {
         printf("File tansfer completed successfully.\n");
         returnValue = DONE;
      }
   }

   return returnValue;
}


void check_args(int argc, char ** argv)
{
   if (argc != 8) {
      printf("Usage %s fromFile toFile win_size buffer_size error_rate hostname port\n", argv[0]);
      exit(-1);
   }

   if (strlen(argv[1]) > 100) {
      printf("FROM filename to long needs to be less than 1000 and is %d\n", strlen(argv[1]));
      exit(-1);
   }
   if (strlen(argv[2]) > 100) {
      printf("TO filename to long needs to be less than 1000 and is %d\n", strlen(argv[2]));
      exit(-1);
   }
   if (atoi(argv[3]) < 1) {
      printf("Window size needs to be be greater than 0 and is %d\n", strlen(argv[3]));
      exit(-1);
   }
   if (atoi(argv[4]) < 400 || atoi(argv[4]) > 1500) {
      printf("Buffer size needs to be between 400 and 1500 and is and is %d\n", strlen(argv[4]));
      exit(-1);
   }
   if (atoi(argv[5]) < 0 || atoi(argv[5]) >= 1) {
      printf("error rate needs to be between 0 and 1 and is %d\n", atoi(argv[5]));
      exit(-1);
   }
}
