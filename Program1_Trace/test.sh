#!/bin/bash

function test {
   echo $1
   ./trace ./trace_files/$1.pcap > ./trace_files/$1_test.out
   diff -q -B --ignore-all-space ./trace_files/$1_test.out ./trace_files/$1.out
   echo $1" test complete"
   echo ""
}

rm test.out
make

echo ""

test "ArpTest"
test "Http"
test "IP_bad_checksum"
test "largeMix"
test "largeMix2"
test "PingTest"
test "smallTCP"
test "TCP_bad_checksum"
test "UDPfile"
