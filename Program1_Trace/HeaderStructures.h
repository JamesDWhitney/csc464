#ifndef _HEADERSTRUCTURES_H
#define _HEADERSTRUCTURES_H

#include <stdint.h>


typedef struct {
   u_int8_t  dst[6];
   u_int8_t  src[6];
   u_int16_t type;
}__attribute__((packed)) Eth_Header;


typedef struct {
   u_int8_t version_IHL;
   u_int8_t tos; //last 2 are ECN
   u_int16_t length;
   u_int16_t id;
   u_int16_t flag_frag;
   u_int8_t ttl;
   u_int8_t protocol;
   u_int16_t checksum;
   u_int32_t sender;
   u_int32_t dest;
}__attribute__((packed)) IP_Header;


typedef struct {
   u_int16_t hw_type;
   u_int16_t ip_type;
   u_int8_t mac_addr_length;
   u_int8_t ip_addr_length;
   u_int16_t opcode;
   u_int8_t sender_mac_addr[6];
   u_int32_t sender_ip_addr;
   u_int8_t target_mac_addr[6];
   u_int32_t target_ip_addr;
}__attribute__((packed)) ARP_Header;


typedef struct {
   u_int16_t src;
   u_int16_t des;
   u_int32_t seq;
   u_int32_t ack;
   u_int8_t offset;
   u_int8_t flags;
   u_int16_t window;
   u_int16_t checksum;
   u_int16_t urgent_pointer;
   u_int32_t option_padding1;
   u_int32_t option_padding2;
}__attribute__((packed)) TCP_Header;


typedef struct {
   u_int32_t src;
   u_int32_t des;
   u_int8_t reserved;
   u_int8_t protocol;
   u_int16_t tcp_length;
}__attribute__((packed)) Psuedo_Header;


typedef struct {
   u_int16_t src;
   u_int16_t des;
   u_int16_t length;
   u_int16_t checksum;
}__attribute__((packed)) UDP_Header;


typedef struct {
   u_int8_t type;
   u_int8_t code;
   u_int16_t checksum;
}__attribute__((packed)) ICMP_Header;


#endif
