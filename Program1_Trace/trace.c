// James Whitney CPE464 Noon lab

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netinet/ether.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pcap.h>

#include "HeaderStructures.h"
#include "checksum.h"

#define TRUE 1
#define FALSE 0

#define IP_TYPE 0x800
#define ARP_TYPE 0x806

#define ICMP_PROTO 1
#define TCP_PROTO 6
#define UDP_PROTO 17

#define ICMP_ECHO_REQ 8
#define ICMP_ECHO_REP 0

#define ARP_REQ 1
#define ARP_REP 2


void printNamedPortTCP(u_int16_t port) {
   switch(port) {
      case 20:
      case 21:
      printf("FTP\n");
      break;
      case 23:
      printf("Telnet\n");
      break;
      case 25:
      printf("SMTP\n");
      break;
      case 53:
      printf("DNS\n");
      break;
      case 80:
      printf("HTTP\n");
      break;
      case 110:
      printf("POP3\n");
      break;
      default:
      printf("%u\n", port);
      break;
   }
}


void printNamedPortUDP(u_int16_t port) {
   switch(port) {
      case 67:
      case 68:
      printf("DHCP\n");
      break;
      case 69:
      printf("TFTP\n");
      break;
      case 123:
      printf("NTP\n");
      break;
      case 53:
      printf("DNS\n");
      break;
      default:
      printf("%u\n", port);
      break;
   }
}


u_int16_t parse_Eth_Header(const u_char * data) {
   Eth_Header *header = (Eth_Header *) data;
   printf("\n\tEthernet Header\n");
   printf("\t\tDest MAC: %s\n", ether_ntoa((const struct ether_addr *)header->dst));
   printf("\t\tSource MAC: %s\n", ether_ntoa((const struct ether_addr *)header->src));
   return ntohs(header->type);
}


void parse_TCP_Header(const u_char * data, int ip_header_length) {
   IP_Header *ip_header = (IP_Header *) data;
   TCP_Header *header = (TCP_Header *) (data + ip_header_length);
   printf("\n\tTCP Header\n");
   printf("\t\tSource Port:  ");
   printNamedPortTCP(ntohs(header->src));
   printf("\t\tDest Port:  ");
   printNamedPortTCP(ntohs(header->des));

   printf("\t\tSequence Number: %u\n", ntohl(header->seq));
   printf("\t\tACK Number: %u\n", ntohl(header->ack));
   int header_length = (header->offset >> 4 ) * 4;
   printf("\t\tData Offset (bytes): %u\n", header_length);
   printf("\t\tSYN Flag: %s\n", ( header->flags & 0x02 ? "Yes" : "No") );
   printf("\t\tRST Flag: %s\n", ( header->flags & 0x04 ? "Yes" : "No") );
   printf("\t\tFIN Flag: %s\n", ( header->flags & 0x01 ? "Yes" : "No") );
   printf("\t\tACK Flag: %s\n", ( header->flags & 0x10 ? "Yes" : "No") );
   printf("\t\tWindow Size: %u\n", ntohs(header->window));   

   //Checksum psuedo_header construction
   u_int16_t tcp_packet_size = ntohs(ip_header->length) - ip_header_length;

   //BUILD PSUEDO HEADER
   u_char *psuedo_buffer = calloc(0, sizeof(Psuedo_Header) + tcp_packet_size );
   if (psuedo_buffer == NULL) {
      fprintf(stderr, "Malloc Failure.\n");
      exit(EXIT_FAILURE);
   }
   //build static object, does work if you just cast the data, prob byte alignment
   Psuedo_Header pHeader;
   pHeader.src = ip_header->sender;
   pHeader.des = ip_header->dest;
   pHeader.reserved = (u_int8_t)0.0;
   pHeader.protocol = ip_header->protocol;
   pHeader.tcp_length = htons(tcp_packet_size);

   //Copy into the buffer
   psuedo_buffer = memcpy(psuedo_buffer, &pHeader, sizeof(pHeader));
   memcpy(psuedo_buffer + sizeof(pHeader), header, tcp_packet_size);

   u_int16_t cksum = in_cksum((unsigned short *) psuedo_buffer, sizeof(Psuedo_Header) + tcp_packet_size);

   if (cksum == 0) {
      printf("\t\tChecksum: Correct (0x%.4x)\n", ntohs(header->checksum));
   }
   else {
      printf("\t\tChecksum: Incorrect (0x%.4x)\n", ntohs(header->checksum));
   }
} 


void parse_UDP_Header(const u_char *data) {
   UDP_Header *header = (UDP_Header *) data;
   printf("\n\tUDP Header\n");
   printf("\t\tSource Port:  ");
   printNamedPortUDP(ntohs(header->src));
   printf("\t\tDest Port:  ");
   printNamedPortUDP(ntohs(header->des));
} 


void parse_ICMP_Header(const u_char *data) {
   ICMP_Header *header = (ICMP_Header * ) data;
   printf("\n\tICMP Header\n");
   switch(header->type) {
      case ICMP_ECHO_REQ:
      printf("\t\tType: Request\n");
      break;
      case ICMP_ECHO_REP:
      printf("\t\tType: Reply\n"); 
      break;
      default:
      printf("\t\tType: %u\n", header->type); 
      break;
   }
} 


void parse_IP_Header(const u_char *data) {
   IP_Header *header = (IP_Header *) data;
   printf("\n\tIP Header\n");
   printf("\t\tIP Version: %u\n", header->version_IHL >> 4);
   int header_length = (header->version_IHL & 0xF) * 4;
   printf("\t\tHeader Len (bytes): %u\n", header_length);
   printf("\t\tTOS subfields:\n");
   printf("\t\t\tDiffserv bits: %u\n", header->tos >> 2);
   printf("\t\t\tECN bits: %u\n", header->tos & 0x3);
   printf("\t\tTTL: %u\n", header->ttl);

   u_int8_t protocol = header->protocol;
   switch(protocol){
      case TCP_PROTO:
      printf("\t\tProtocol: TCP\n");
      break;
      case UDP_PROTO:
      printf("\t\tProtocol: UDP\n");
      break;
      case ICMP_PROTO:
      printf("\t\tProtocol: ICMP\n");
      break;
      default:
      printf("\t\tProtocol: Unknown\n");
      break;
   }

   unsigned short cksum = in_cksum((unsigned short *)header, header_length);
   if (cksum == 0) {
      printf("\t\tChecksum: Correct (0x%.4x)\n", ntohs(header->checksum));
   }
   else {
      printf("\t\tChecksum: Incorrect (0x%.4x)\n", ntohs(header->checksum));
   }

   printf("\t\tSender IP: %s\n", inet_ntoa( *((struct in_addr *) &(header->sender)) ));
   printf("\t\tDest IP: %s\n", inet_ntoa( *((struct in_addr *) &(header->dest)) ));

   switch(protocol){
      case TCP_PROTO:
      parse_TCP_Header(data, header_length);
      break;
      case UDP_PROTO:
      parse_UDP_Header(data + header_length);
      break;
      case ICMP_PROTO:
      parse_ICMP_Header(data + header_length);
      break;
   }
}


void parse_ARP_Header(const u_char *data) {
   ARP_Header *header = (ARP_Header *) data;
   printf("\n\tARP header\n");
   if (ntohs(header->opcode) == ARP_REQ) {
      printf("\t\tOpcode: Request\n");
   }
   else if (ntohs(header->opcode) == ARP_REP) {
      printf("\t\tOpcode: Reply\n");
   }
   else {
      printf("\t\tOpcode: Unknown\n");
   }
   printf("\t\tSender MAC: %s\n", ether_ntoa((const struct ether_addr *)header->sender_mac_addr));
   printf("\t\tSender IP: %s\n", inet_ntoa( *((struct in_addr *) &(header->sender_ip_addr)) ));
   printf("\t\tTarget MAC: %s\n", ether_ntoa((const struct ether_addr *)header->target_mac_addr));
   printf("\t\tTarget IP: %s\n\n", inet_ntoa( *((struct in_addr *) &(header->target_ip_addr)) ));
}


int main(int argc, char *argv[]) {
   //Check Arguments
   if (argc != 2) {
      fprintf(stderr, "Usage: trace <Filename.pcap>\n");
      exit(EXIT_FAILURE);
   }
   //Open PCAP file
   char errBuf[PCAP_ERRBUF_SIZE];
   pcap_t * handle = pcap_open_offline(argv[1], errBuf);
   if (handle == NULL) {
      fprintf(stderr, "Invalid input file.\n");
      exit(EXIT_FAILURE);
   }

   //Parse Headers
   const u_char *data;
   struct pcap_pkthdr *header; 

   int packet_num = 1;
   while(pcap_next_ex(handle, &header, &data) == 1) {
      printf("\nPacket number: %u  Packet Len: %u\n", packet_num, header->len);
      u_int16_t type = parse_Eth_Header(data);
      switch(type){
            case IP_TYPE:
            //protocols undeer IP are called in IP due to needed parameters
            printf("\t\tType: IP\n");
            parse_IP_Header(data + sizeof(Eth_Header));
            break;

            case ARP_TYPE:
            printf("\t\tType: ARP\n");
            parse_ARP_Header(data + sizeof(Eth_Header));
            break;

            default:
            printf("\t\tType: Unknown\n");
            printf("\n\tUnknown PDU\n");
            break;
      }
      packet_num++;
   }
   pcap_close(handle);
   return EXIT_SUCCESS;
}